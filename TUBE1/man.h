#pragma once
#include "QuUtils.h"
#include "QuCamera.h"
#include "QuInterface.h"
#include "QuGameManager.h"

class TuMenuMan : public QuBaseManager
{
public:
	TuMenuMan();
	virtual ~TuMenuMan();
	virtual void initialize();
	virtual void update();
	virtual void keyDown(int key){}
	virtual void keyUp(int key){}
	virtual void singleDown(int button, QuScreenCoord crd);
	virtual void singleUp(int button, QuScreenCoord crd);
	virtual void singleMotion(QuScreenCoord crd);
};
