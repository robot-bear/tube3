#include "man.h"
#include "IwUI.h"


//Simple handler to quit application on any button press
class CHelloWorldHandler : public IIwUIEventHandler
{
    virtual bool HandleEvent(CIwEvent* pEvent)
    {
        return false;
    }

    virtual bool FilterEvent(CIwEvent* pEvent)
    {
        if( pEvent->GetID() == IWUI_EVENT_BUTTON )
        {
            s3eDeviceRequestQuit();
            return true;
        }

        return false;
    }
} g_HelloWorldHandler;




TuMenuMan::TuMenuMan(){}
TuMenuMan::~TuMenuMan()
{
	delete IwGetUIController();
    delete IwGetUIView();

    //Terminate the IwUI module
    IwUITerminate();
}
void TuMenuMan::initialize()
{
	//Initialise the IwUI module
    IwUIInit();

    //Instantiate the view and controller singletons.
    //IwUI will not instantiate these itself, since they can be subclassed to add functionality.
    new CIwUIController;
    new CIwUIView;

    //Provide global event handler
    IwGetUIController()->AddEventHandler(&g_HelloWorldHandler);

	 //Load the hello world UI
    IwGetResManager()->LoadGroup("IwUIHelloWorld.group");

    //Set the default style sheet
    CIwResource* pResource = IwGetResManager()->GetResNamed("iwui", IW_UI_RESTYPE_STYLESHEET);
    IwGetUIStyleManager()->SetStylesheet(IwSafeCast<CIwUIStylesheet*>(pResource));

    //Find the dialog template
    CIwUIElement* pDialogTemplate = (CIwUIElement*)IwGetResManager()->GetResNamed("Vertical", "CIwUIElement");

    //And instantiate it
    CIwUIElement* pDialog = pDialogTemplate->Clone();
    IwGetUIView()->AddElement(pDialog);
    IwGetUIView()->AddElementToLayout(pDialog);

	new CIwUITextInput;
	IwGetUITextInput()->CreateSoftKeyboard();
	IwGetUIView()->GetChildNamed("soft_keyboard")->SetRenderSlot(INT32_MAX-1);

}

void TuMenuMan::update()
{
	//Update the controller (this will generate control events etc.)
    IwGetUIController()->Update();

    //Update the view (this will do animations etc.) Airplay SDK's example framework has a fixed
    //framerate of 20fps, so we pass that duration to the update function.
    IwGetUIView()->Update(1000/20);

	//Render the UI
    IwGetUIView()->Render();

    //Flush IwGx
    IwGxFlush();
    //Display the rendered frame
    //IwGxSwapBuffers();


}
void TuMenuMan::singleDown(int button, QuScreenCoord crd){}
void TuMenuMan::singleUp(int button, QuScreenCoord crd){}
void TuMenuMan::singleMotion(QuScreenCoord crd){}